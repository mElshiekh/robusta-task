//
//  NetworkManager.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import Foundation
import RxSwift

class NetworkManager {
    static let shared = NetworkManager()

    private init() {}

    func processGetReq<T>(url: String, returnType: T.Type) -> Observable<T?> where T: Codable {
        let objResponse = PublishSubject<T?>()
        if let url = URL(string: url) {
            let task = URLSession.shared.dataTask(with: url) { [weak self] data, _, _ in
                guard let data = data else { return }
                self?.parseResponse(data: data, returnType: returnType, objResponse: objResponse)
            }
            task.resume()
        }
        return objResponse
    }

    fileprivate func parseResponse<T>(data: Data?, returnType: T.Type, objResponse: PublishSubject<T?>) where T: Codable {
        if let _ = data {
            do {
                let response = try JSONDecoder().decode(T.self, from: data!)
                objResponse.onNext(response)
                objResponse.onCompleted()
            } catch {
                handleError(message: "Server issue", objResponse: objResponse)
            }
        }
    }

    func handleError<T>(message: String, objResponse: PublishSubject<T?>) {
        objResponse.onNext(nil)
        objResponse.onError(NetWorkError(message: message))
        objResponse.onCompleted()
    }
}
