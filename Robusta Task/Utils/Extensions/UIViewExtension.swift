//
//  UIViewExtension.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import UIKit

extension UIView {
    func fillSuperView(shouldUseSafeArea: Bool = true, padding: UIEdgeInsets = UIEdgeInsets.zero) {
        guard let superview = superview else { return }
        translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            leadingAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.leadingAnchor :
                superview.leadingAnchor, constant: padding.left),
            trailingAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.trailingAnchor :
                superview.trailingAnchor, constant: -padding.right),

            topAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.topAnchor :
                superview.topAnchor, constant: padding.top),
            bottomAnchor.constraint(equalTo: shouldUseSafeArea ?
                superview.safeAreaLayoutGuide.bottomAnchor :
                superview.bottomAnchor, constant: -padding.bottom),
        ])
    }
}

extension UIView {
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        } set {
            layer.cornerRadius = newValue
        }
    }
}
