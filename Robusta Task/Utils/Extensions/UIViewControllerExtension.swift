//
//  UIViewControllerExtension.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/29/21.
//

import UIKit


extension UIViewController {
    
    func instantiateViewController(viewControllerId: ViewControllerID, StoryboardId: StoryboardID) -> UIViewController? {
        let storyboard = UIStoryboard(name: StoryboardId.rawValue, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: viewControllerId.rawValue)
        return controller
    }
}

extension UIViewController {
    func addNoItemsView(on holdingView: UIView, message: String) {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.textAlignment = .center
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        l.font = UIFont.systemFont(ofSize: 20)
        l.text = message
        l.tag = ViewTags.NoItemsView.rawValue
        holdingView.addSubview(l)
        l.fillSuperView()
    }

    func removeNoItemsView(from holdingView: UIView) {
        if let v = holdingView.viewWithTag(ViewTags.NoItemsView.rawValue) {
            v.isHidden = true
            v.removeFromSuperview()
        }
        if let _ = holdingView.viewWithTag(ViewTags.NoItemsView.rawValue) {
            removeNoItemsView(from: holdingView)
        }
    }
}

extension UIViewController {
    func showSpinner(onView: UIView, backColor: UIColor = UIColor.black.withAlphaComponent(0)) {
        let spinnerView = UIView(frame: onView.frame)
        spinnerView.backgroundColor = backColor
        spinnerView.translatesAutoresizingMaskIntoConstraints = false
        //
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = CGRect(x: 0, y: 0, width: 100, height: 100)
        blurEffectView.cornerRadius = 10
        blurEffectView.clipsToBounds = true
        spinnerView.addSubview(blurEffectView)
        blurEffectView.center = spinnerView.center
        //
        var ai = UIActivityIndicatorView()
        if #available(iOS 13, *) {
            ai = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.large)
        } else {
            ai = UIActivityIndicatorView(style: .whiteLarge)
        }
        ai.color = .white
        ai.startAnimating()

        DispatchQueue.main.async { [weak self] in
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
            spinnerView.fillSuperView()
            self?.view.layoutIfNeeded()
            blurEffectView.center = spinnerView.center
            ai.center = spinnerView.center
            self?.view.isUserInteractionEnabled = false
            self?.navigationController?.view.isUserInteractionEnabled = false
        }

        spinnerView.tag = ViewTags.SpinnerTag.rawValue
    }

    func removeSpinner(fromView: UIView) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.1) {
            var loader: UIView? = fromView.viewWithTag(ViewTags.SpinnerTag.rawValue)
            UIView.animate(withDuration: 0.2, animations: {
                loader?.alpha = 0
            }, completion: { [weak self] _ in
                loader?.removeFromSuperview()
                loader = nil
                self?.view.isUserInteractionEnabled = true
                self?.navigationController?.view.isUserInteractionEnabled = true
            })
        }
    }

    func removeSpinnerImmediatly(fromView: UIView) {
        DispatchQueue.main.async {
            var loader: UIView? = fromView.viewWithTag(ViewTags.SpinnerTag.rawValue)
            loader?.removeFromSuperview()
            loader = nil
        }
    }
}

class LoadableScreen: UIViewController {
    var isLoading = false

    func showSpinnerOnMainView() {
        showSpinner(onView: view)
        isLoading = true
    }

    func removeSpinnerFromMainView() {
        removeSpinner(fromView: view)
        isLoading = false
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        if isLoading {
            removeSpinnerImmediatly(fromView: view)
            print("Landscape \(UIDevice.current.orientation.isLandscape)")
            showSpinnerOnMainView()
        }
    }
}
