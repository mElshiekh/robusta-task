//
//  UIImageViewExtension.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import UIKit

extension UIImageView {
    func setImage(from urlString: String?) {
        if let urlString = urlString, let url = URL(string: urlString) {
            Data.getData(from: url) { data, response, error in
                guard let data = data, error == nil else { return }
                DispatchQueue.main.async { [weak self] in
                    self?.image = UIImage(data: data)
                }
            }
        }
    }
}
