//
//  DataExtension.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import Foundation

extension Data {
    static func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> Void) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
}
