//
//  Constants.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import Foundation

enum StoryboardID: String {
    case Repositories
}

enum ViewControllerID: String {
    case ListViewController
    case DetailsViewController
}

class BaseUrls {
    static var base: String {
        let url = "https://api.github.com/"
        return url
    }
}

enum EndPointUrls: String {
    case repositories = "repositories"
}

enum ViewTags: Int {
    case NoItemsView = 707070700
    case SpinnerTag = 101010101014510
}
