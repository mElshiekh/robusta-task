//
//  ServiceType.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import Foundation

class ServiceType {
    var networkManager = NetworkManager.shared
    
    func getFullUrl(baseUrl: String, endPoint: EndPointUrls, byAppending items: [String] = []) -> String {
        let urlString = "\(baseUrl)\(endPoint.rawValue)"
        let fullUrl = String(format: urlString, arguments: items)
        return fullUrl
    }
}
