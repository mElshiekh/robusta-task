//
//  NetworkError.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import Foundation
struct NetWorkError: Error {
    var message: String?
}
