//
//  RepositoryServices.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import Foundation
import RxSwift

protocol RepositoryServicesType {
    func getRepositories() -> Observable<[Repository]?>
}

class RepositoryServices: ServiceType, RepositoryServicesType {
    func getRepositories() -> Observable<[Repository]?> {
        let url = getFullUrl(baseUrl: BaseUrls.base, endPoint: .repositories)
        return networkManager.processGetReq(url: url, returnType: [Repository].self)
    }
}
