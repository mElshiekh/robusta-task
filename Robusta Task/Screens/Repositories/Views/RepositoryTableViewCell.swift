//
//  RepositoryTableViewCell.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import UIKit

class RepositoryTableViewCell: UITableViewCell {
    static let identifier = "RepositoryTableViewCell"
    
    
    @IBOutlet var avatarImage: UIImageView!
    @IBOutlet var dateLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var ownerLabel: UILabel!
    
    func setupWith(repo: Repository) {
        avatarImage.setImage(from: repo.owner?.avatarURL)
        dateLabel.text = "no date"
        nameLabel.text = repo.name
        ownerLabel.text = repo.owner?.login
    }
    
}
