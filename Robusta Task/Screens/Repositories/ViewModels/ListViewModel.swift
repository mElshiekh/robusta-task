//
//  ListViewModel.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import RxCocoa
import RxSwift

class ListViewModel {
    // MARK: - variables

    private var disposeBag = DisposeBag()
    private var apiService: RepositoryServicesType = RepositoryServices()
    var repositoriesViewData = BehaviorRelay<[Repository]>(value: [])
    var isLoadingViewData = BehaviorRelay<Bool?>(value: nil)
    var errorViewData = BehaviorRelay<NetWorkError?>(value: nil)
    
    var repositories = [Repository]()
    
    func getRepositories() {
        isLoadingViewData.accept(true)
        apiService.getRepositories().asObservable().subscribe(onNext: { [weak self] response in
            self?.isLoadingViewData.accept(false)
            if let resp = response {
                self?.repositories = resp
                self?.repositoriesViewData.accept(resp)
            }
        }, onError: { [weak self] respError in
            self?.isLoadingViewData.accept(false)
            if let error = respError as? NetWorkError {
                self?.errorViewData.accept(error)
            }
            
        }).disposed(by: disposeBag)
    }
}

