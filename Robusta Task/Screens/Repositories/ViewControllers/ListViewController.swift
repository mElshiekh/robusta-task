//
//  ListViewController.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/28/21.
//

import RxCocoa
import RxSwift
import UIKit

class ListViewController: LoadableScreen {
    @IBOutlet var searchField: UITextField!
    @IBOutlet var tableView: UITableView!

    var viewModel = ListViewModel()
    private var disposeBag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        bindViewData()
    }
}

extension ListViewController {
    private func setupUI() {
        setupTableView()
    }

    private func setupTableView() {
        tableView.register(UINib(nibName: RepositoryTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: RepositoryTableViewCell.identifier)
    }
}

extension ListViewController {
    private func bindViewData() {
        viewModel.repositoriesViewData.bind(to: tableView.rx.items(cellIdentifier: RepositoryTableViewCell.identifier)) { _, model, cell in
            (cell as! RepositoryTableViewCell).setupWith(repo: model)
        }
        .disposed(by: disposeBag)

        tableView.rx.itemSelected.subscribe(onNext: { [weak self] indexPath in
            guard let self = self else { return }
            let item = self.viewModel.repositoriesViewData.value[indexPath.row]
            if let vc = self.instantiateViewController(viewControllerId: .DetailsViewController, StoryboardId: .Repositories) as? DetailsViewController {
                vc.repository = item
                self.navigationController?.pushViewController(vc, animated: true)
            }
        }).disposed(by: disposeBag)

        viewModel.repositoriesViewData.asObservable().subscribe(onNext: { [weak self] repos in
            DispatchQueue.main.async { [weak self] in
                guard let self = self else { return }
                if repos.count > 0 {
                    self.removeNoItemsView(from: self.tableView)
                } else {
                    self.addNoItemsView(on: self.tableView, message: "no Repositories")
                }
            }
        }).disposed(by: disposeBag)

        searchField.rx.text.map({ (value) -> String? in
            ((value?.count ?? 0) < 2) ? nil : value
        }).asDriver(onErrorJustReturn: nil)
            .distinctUntilChanged().drive(onNext: { [weak self] query in
                guard let self = self else { return }
                if let q = query {
                    let filteredRepos = self.viewModel.repositories.filter({ ($0.name?.contains(q) ?? false) })
                    self.viewModel.repositoriesViewData.accept(filteredRepos)
                } else {
                    self.viewModel.repositoriesViewData.accept(self.viewModel.repositories)
                }
            }).disposed(by: disposeBag)

        viewModel.isLoadingViewData.asObservable().subscribe(onNext: { [weak self] response in
            guard let self = self else { return }
            if response == true {
                self.showSpinnerOnMainView()
            } else if response == false {
                self.removeSpinnerFromMainView()
            }
        }).disposed(by: disposeBag)

        viewModel.errorViewData.asObservable().subscribe(onNext: { data in
            if let error = data?.message {
                print(error)
            }
        }).disposed(by: disposeBag)

        viewModel.getRepositories()
    }
}
