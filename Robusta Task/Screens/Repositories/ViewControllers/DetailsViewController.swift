//
//  DetailsViewController.swift
//  Robusta Task
//
//  Created by Elshiekh on 4/29/21.
//

import UIKit

class DetailsViewController: UIViewController {
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var ownerNameLabel: UILabel!
    @IBOutlet var privacyStatusLabel: UILabel!
    @IBOutlet var descriptionLabel: UILabel!
    var repository: Repository?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
}

extension DetailsViewController {
    private func setupUI() {
        avatarImageView.setImage(from: repository?.owner?.avatarURL)
        privacyStatusLabel.text = (repository?.isPrivate ??  false) ? "Private" : "Public"
        nameLabel.text = repository?.name
        ownerNameLabel.text = repository?.owner?.login
        descriptionLabel.text = repository?.repositoryDescription
    }
}
